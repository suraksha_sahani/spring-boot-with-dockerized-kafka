package com.demo.kafka.springbootkafka.controller;

import com.demo.kafka.springbootkafka.model.User;
import com.demo.kafka.springbootkafka.producer.Producer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/kafka")
public class KafkaTestController {

    @Autowired
    private Producer producer;

//    @Autowired
//    KafkaTestController(Producer producer) {
//        this.producer = producer;
//    }

    @PostMapping(value = "/publish")
    public ResponseEntity sendMessageToKafkaTopic(@RequestParam("message") String message) {
        this.producer.sendMessage(message);
        Map<String, String> map = new HashMap<>();
        map.put("message","Message has been sent successfully!");
        return ResponseEntity.status(HttpStatus.OK).body(map);
    }

    @PostMapping(value = "/publish-json")
    public ResponseEntity<String> sendMessageJSON(@RequestBody User user) {
        this.producer.sendMessage(user);
        return  ResponseEntity.ok("SENT!");
    }
}
