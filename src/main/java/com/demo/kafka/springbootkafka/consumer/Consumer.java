package com.demo.kafka.springbootkafka.consumer;

import com.demo.kafka.springbootkafka.model.User;
import com.demo.kafka.springbootkafka.producer.Producer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class Consumer {

    private final Logger logger = LoggerFactory.getLogger(Producer.class);

    @KafkaListener(topics = "users")
    public void consume(String message) throws IOException {
        logger.info(String.format("#### -> Consumed message -> %s", message));
    }
    @KafkaListener(topics = "users-json")
    public void consume(User data){
        logger.info(String.format("Message received -> %s", data.toString()));
    }



}
