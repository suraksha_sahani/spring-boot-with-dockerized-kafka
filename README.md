# Spring boot with dockerized kafka



## Getting started

Here are some steps to run kafka locally 

### Step 1: 
    docker ps
To make sure if ports (i.e. 9092 and 2181) ae not allocated by any other services and if they are already allocated then you need to kill processes first.

    docker-compose up -d

Run both the services kafka and zookeeper locally on your machine 

    docker ps
Now your ports should be allocated and running 


### Step 2: 
Now run your application locally and check following endpoints

Endpoint 1: To check the string message
    
    URL : http://localhost:9000/kafka/publish?message=Hello, I am testing this message to check the listener
    METHOD: POST 
    
    

Response: Check you application must be listening messages to the specified topics

Endpoint 2: To check json based serialized response
    URL : http://localhost:9000/kafka/publish-json
    METHOD: POST 

    Request Body : 
    {
    "id":1,
    "firstName":"Spring",
    "lastName":"Kafka"    
    }

Response: Check you application must be listening messages to the specified topics
